NAME     := $(shell basename $(CURDIR))
IPATH    := gitlab.com/latency.at/$(NAME)

REVISION := $(shell git rev-parse HEAD | cut -c1-8)
BRANCH   := $(shell git symbolic-ref --short -q HEAD)
DATE     := $(shell date +%Y%m%d-%H%M%S)

REGISTRY ?= registry.gitlab.com
CI_PROJECT_NAME      ?= latency.at
CI_PROJECT_NAMESPACE ?= latencyat

DOCKER_BASE  ?= $(CI_REGISTRY_IMAGE)
DOCKER_BASE  ?= registry.gitlab.com/latency.at/latencyat
DOCKER_IMAGE ?= $(DOCKER_BASE):$(REVISION)

FNAME := $(NAME)-$(BRANCH)_$(REVISION)_$(DATE).tar.gz

CA_BUNDLE ?= /etc/ssl/certs/ca-certificates.crt
SOURCES   := $(shell find . -name '*.go')
BINS      := cmd/api/api cmd/booker/booker cmd/balancer/balancer

VAT_RATES_URL ?= https://raw.githubusercontent.com/kdeldycke/vat-rates/master/vat_rates.csv

all: build
build: dist/

$(BINS): $(SOURCES)
	cd $(dir $@) && CGO_ENABLED=0 go build -ldflags "\
		-X $(IPATH)/version.Revision=${REVISION} \
		-X $(IPATH)/version.Branch=${BRANCH} \
		-X $(IPATH)/version.BuildDate=${DATE} \
	" .

$(FNAME): $(BINS) sql/ Makefile
	tar -czf $@ $^

dist/: $(BINS) sql/ $(CA_BUNDLE) templates/
	mkdir -p dist/etc
	curl -Lo dist/etc/vat_rates.csv $(VAT_RATES_URL)
	rsync -avR $^ dist/

init: sql/database.sql
	psql < $<
	echo 'CREATE EXTENSION IF NOT EXISTS citext;' \
		| psql latencyat

schema: sql/schema.sql
	psql latencyat < $<

dredd: cmd/dredd-hook/dredd-hook $(SOURCES)
	dredd docs/swagger.yml http://127.0.0.1:8000 --language=go --hookfiles=$<

vet:
	go vet $(shell go list ./... | grep -v /vendor/)

clean:
	rm -rf $(BINS) templates/*.html.tmpl

test: vet
	go test ./...

docker-builder:
	docker build -t $(DOCKER_IMAGE)-builder .

docker-builder-push: docker-builder
	docker push $(DOCKER_IMAGE)-builder

latencyAt-rootfs.tar: docker-builder
	docker run $(DOCKER_IMAGE)-builder tar -cC dist/ . > latencyAt-rootfs.tar

docker-import: latencyAt-rootfs.tar
	docker import - $(DOCKER_IMAGE) < $<

docker-push: docker-import
	docker push $(DOCKER_IMAGE)

gl-build:
	gitlab-ci-multi-runner-linux-amd64 exec docker build \
		--env CI_PROJECT_NAME=latencyAt \
		--env CI_PROJECT_NAMESPACE=latency.at \
		--env CI_COMMIT_SHA=$(REVISION)

.PHONY: init schema templates build deploy test vet
