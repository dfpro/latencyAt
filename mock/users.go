package mock

import (
	"io"

	"gitlab.com/latency.at/latencyAt"
)

type UserService struct {
	UserFn      func(id int) (*latencyAt.User, error)
	UserInvoked bool

	UserByEmailFn func(email string) (*latencyAt.User, error)

	CreateFn      func(user *latencyAt.User) error
	CreateInvoked bool

	BalanceFn      func(user *latencyAt.User, balance int) error
	BalanceInvoked bool

	BalanceAbsFn func(user *latencyAt.User, balance int) error

	ActivateFn      func(user *latencyAt.User) error
	ActivateInvoked bool

	SetEmailAndDeactivateFn      func(id int, email string) error
	SetEmailAndDeactivateInvoked bool

	SetStripeCustomerIDFn func(id int, customerID string) error

	UserByStripeCustomerIDFn func(id string) (*latencyAt.User, error)

	BalanceAbsByStripeCustomerIDFn func(id string, balance int) error
	BalanceByStripeCustomerIDFn    func(id string, balance int) error

	UserFromJSONFn func(r io.Reader) (*latencyAt.User, error)
}

func (s *UserService) User(id int) (*latencyAt.User, error) {
	s.UserInvoked = true
	return s.UserFn(id)
}

func (s *UserService) Create(user *latencyAt.User) error {
	s.CreateInvoked = true
	return s.CreateFn(user)
}

func (s *UserService) Balance(user *latencyAt.User, balance int) error {
	s.BalanceInvoked = true
	return s.BalanceFn(user, balance)
}

func (s *UserService) BalanceAbs(user *latencyAt.User, balance int) error {
	return s.BalanceAbsFn(user, balance)
}

func (s *UserService) UserByStripeCustomerID(id string) (*latencyAt.User, error) {
	return s.UserByStripeCustomerIDFn(id)
}

func (s *UserService) Activate(user *latencyAt.User) error {
	s.ActivateInvoked = true
	return s.ActivateFn(user)
}

func (s *UserService) SetEmailAndDeactivate(id int, email string) error {
	s.SetEmailAndDeactivateInvoked = true
	return s.SetEmailAndDeactivateFn(id, email)
}

func (s *UserService) UserByEmail(email string) (*latencyAt.User, error) {
	return s.UserByEmailFn(email)
}

func (s *UserService) UserByEmailTokenID(id int) (*latencyAt.User, error) {
	panic("not implemented")
}

func (s *UserService) SetPasswordHash(id int, passwordHash []byte) error {
	panic("not implemented")
}

func (s *UserService) UserFromJSON(r io.Reader) (*latencyAt.User, error) {
	return s.UserFromJSONFn(r)
}

func (s *UserService) SetStripeCustomerID(id int, customerID string) error {
	return s.SetStripeCustomerIDFn(id, customerID)
}

func (s *UserService) Purge() error {
	panic("not implemented")
}

func (s *UserService) Healthy() error {
	panic("not implemented")
}

func (s *UserService) BalanceAbsByStripeCustomerID(id string, balance int) error {
	return s.BalanceAbsByStripeCustomerIDFn(id, balance)
}

func (s *UserService) BalanceByStripeCustomerID(id string, balance int) error {
	return s.BalanceByStripeCustomerIDFn(id, balance)
}

func (s *UserService) SetVatIn(uid int, vatin string) error {
	panic("not implemented")
}

func (s *UserService) UpdateStats() {
	panic("not implemented")
}

func (s *UserService) IncRequests(uid int, inc int) error {
	panic("not implemented")
}
