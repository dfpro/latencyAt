package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/dannyvankooten/vat"
	"github.com/julienschmidt/httprouter"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"
)

func (a *APIHandler) handleSignup(r *http.Request, _ httprouter.Params) (interface{}, error) {
	ruser, err := latencyAt.UserFromJSON(r.Body)
	if err != nil {
		return nil, err
	}
	if err := validate(ruser.Email, ruser.Password); err != nil {
		return nil, &errors.Error{Err: err, StatusCode: http.StatusBadRequest}
	}

	_, err = a.UserService.UserByEmail(ruser.Email)
	if err == nil {
		return nil, errors.ErrUserEmailExists
	}
	if err != errors.ErrUserNotFound {
		return nil, err
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(ruser.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	ruser.PasswordHash = passwordHash
	ruser.Balance = latencyAt.DefaultBalance

	emailToken, err := a.createEmailToken()
	if err != nil {
		return nil, err
	}
	ruser.EmailTokenID = &emailToken.ID
	ruser.Registered = time.Now()
	if err := a.UserService.Create(ruser); err != nil {
		return nil, err
	}
	if err := a.MailService.SendActivation(ruser, emailToken); err != nil {
		return nil, err
	}
	jwt, err := a.TokenAuth.NewToken(ruser.ID, ruser.Activated)
	if err != nil {
		return nil, err
	}
	return jwt, nil
}

func (a *APIHandler) handleActivateMail(r *http.Request, _ httprouter.Params, userID int) (interface{}, error) {
	user, err := a.UserService.User(userID)
	if err != nil {
		return nil, err
	}
	token, err := a.EmailTokenService.EmailToken(*user.EmailTokenID)
	if err != nil {
		return nil, err
	}
	a.Logger.WithField("user", user).Info("sending activation mail for")
	if err := a.MailService.SendActivation(user, token); err != nil {
		return nil, err
	}
	return &latencyAt.StatusOK, nil
}

func (a *APIHandler) handleResetMail(r *http.Request, ps httprouter.Params) (interface{}, error) {
	ruser, err := latencyAt.UserFromJSON(r.Body)
	if err != nil {
		return nil, err
	}
	log := a.Logger.WithField("email", ruser.Email)
	user, err := a.UserService.UserByEmail(ruser.Email)
	if err != nil {
		if err == errors.ErrUserNotFound {
			log.Info(err)
			return &latencyAt.StatusOK, nil
		}
		return nil, err
	}
	// Should be save to allow unactivated accounts, right?
	/*
		if !user.Activated {
			return nil, &errors.Error{Err: errors.ErrAccountNotActivated, StatusCode: http.StatusForbidden}
		}
	*/

	token, err := a.resetEmailToken(*user.EmailTokenID)
	if err != nil {
		return nil, err
	}

	log.Info("sending reset mail")
	if err := a.MailService.SendReset(user, token); err != nil {
		return nil, err
	}
	return &latencyAt.StatusOK, nil
}

func (a *APIHandler) handleActivate(r *http.Request, ps httprouter.Params, userID int) (interface{}, error) {
	t := ps.ByName("token")
	token, err := a.EmailTokenService.EmailTokenByToken(t)
	if err != nil {
		if err == errors.ErrTokenNotFound {
			return nil, &errors.Error{Err: errors.ErrTokenNotFound, StatusCode: http.StatusForbidden}
		}
		return nil, err
	}
	user, err := a.UserService.UserByEmailTokenID(token.ID)
	if err != nil {
		if err == errors.ErrUserNotFound {
			return nil, &errors.Error{Err: errors.ErrTokenNotFound, StatusCode: http.StatusForbidden}
		}
		return nil, err
	}
	if user.ID != userID {
		return nil, &errors.Error{Err: errors.ErrTokenNotFound, StatusCode: http.StatusForbidden}
	}
	if err := a.UserService.Activate(user); err != nil {
		return nil, err
	}
	return a.TokenAuth.NewToken(user.ID, true)
}

func (a *APIHandler) handleReset(r *http.Request, ps httprouter.Params) (interface{}, error) {
	type passReq struct {
		Password string `json:"password"`
	}
	req := passReq{}
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}
	if err := validatePassword(req.Password); err != nil {
		return nil, &errors.Error{Err: err, StatusCode: http.StatusBadRequest}
	}

	t := ps.ByName("token")
	token, err := a.EmailTokenService.EmailTokenByToken(t)
	if err != nil {
		if err == errors.ErrTokenNotFound {
			return nil, &errors.Error{Err: errors.ErrTokenNotFound, StatusCode: http.StatusForbidden}
		}
		return nil, err
	}
	if token.Created.Before(time.Now().Add(-a.Config.EmailTokenMaxAge)) {
		return nil, &errors.Error{Err: errors.ErrTokenExpired, StatusCode: http.StatusForbidden}
	}

	user, err := a.UserService.UserByEmailTokenID(token.ID)
	if err != nil {
		return nil, err
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	if err := a.UserService.SetPasswordHash(user.ID, passwordHash); err != nil {
		return nil, err
	}
	return &latencyAt.StatusOK, nil
}

func (a *APIHandler) handleLogin(r *http.Request, _ httprouter.Params) (interface{}, error) {
	ruser, err := latencyAt.UserFromJSON(r.Body)
	if err != nil {
		return nil, &errors.Error{Err: err, StatusCode: http.StatusBadRequest}
	}
	user, err := a.UserService.UserByEmail(ruser.Email)
	if err != nil {
		if err == errors.ErrUserNotFound {
			return nil, &errors.Error{
				Err:        errors.ErrCredentialsInvalid,
				StatusCode: http.StatusForbidden,
			}
		}
		return nil, err
	}
	if err := bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(ruser.Password)); err != nil {
		return nil, &errors.Error{
			Err:        errors.ErrCredentialsInvalid,
			StatusCode: http.StatusForbidden,
		}

	}
	token, err := a.TokenAuth.NewToken(user.ID, user.Activated)
	if err != nil {
		return nil, err
	}
	return token, nil
}

func (a *APIHandler) handleUser(r *http.Request, _ httprouter.Params, userID int) (interface{}, error) {
	user, err := a.UserService.User(userID)
	if err != nil {
		return nil, err
	}
	user.Password = "" // We shouldn't return the password to the user
	return user, nil
}

func (a *APIHandler) handleChangePassword(r *http.Request, _ httprouter.Params, uid int) (interface{}, error) {
	type changePasswordRequest struct {
		Password    string `json:"password"`
		NewPassword string `json:"newPassword"`
	}
	req := changePasswordRequest{}
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}

	if err := validatePassword(req.Password); err != nil {
		return nil, &errors.Error{Err: err, StatusCode: http.StatusBadRequest}
	}
	user, err := a.UserService.User(uid)
	if err != nil {
		return nil, err
	}
	if err := bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(req.Password)); err != nil {
		if err == bcrypt.ErrMismatchedHashAndPassword {
			return nil, &errors.Error{Err: errors.ErrCredentialsInvalid, StatusCode: http.StatusForbidden}
		}
		return nil, err
	}
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(req.NewPassword), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	if err := a.UserService.SetPasswordHash(uid, passwordHash); err != nil {
		return nil, err
	}
	return &latencyAt.StatusOK, nil
}

func (a *APIHandler) handleChangeEmail(r *http.Request, _ httprouter.Params, uid int) (interface{}, error) {
	type changeEmailRequest struct {
		Password string `json:"password"`
		NewEmail string `json:"newEmail"`
	}
	req := changeEmailRequest{}
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}
	if err := validateEmail(req.NewEmail); err != nil {
		return nil, &errors.Error{Err: err, StatusCode: http.StatusBadRequest}
	}
	user, err := a.UserService.User(uid)
	if err != nil {
		return nil, err
	}
	if err := bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(req.Password)); err != nil {
		if err == bcrypt.ErrMismatchedHashAndPassword {
			return nil, &errors.Error{Err: errors.ErrCredentialsInvalid, StatusCode: http.StatusForbidden}
		}
		return nil, err
	}
	token, err := a.resetEmailToken(*user.EmailTokenID)
	if err != nil {
		return nil, err
	}
	if err := a.UserService.SetEmailAndDeactivate(uid, req.NewEmail); err != nil {
		return nil, err
	}

	// Send activation email to new address
	user.Email = req.NewEmail
	if err := a.MailService.SendActivation(user, token); err != nil {
		return nil, err
	}
	return &latencyAt.StatusOK, nil
}

func (a *APIHandler) handleChangeVatIn(r *http.Request, _ httprouter.Params, uid int) (interface{}, error) {
	req := struct {
		VatIn string `json:"vatIn"`
	}{}
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}
	if len(req.VatIn) < 3 {
		return nil, fmt.Errorf("Invalid VAT Number: %s", req.VatIn)
	}
	cc := strings.ToUpper(req.VatIn[:2])
	card, err := a.SubscriptionService.GetCard(uid)
	if err != nil {
		return nil, fmt.Errorf("changeVatIn: %s", err)
	}
	if cc != card.CardCountry {
		return nil, fmt.Errorf("VAT Number country (%s) does not match card country (%s)", cc, card.CardCountry)
	}
	ok, err := vat.ValidateNumber(req.VatIn)
	if err != nil {
		return nil, fmt.Errorf("Couldn't validate VAT Number: %s", err)
	}
	if !ok {
		return nil, &errors.Error{Err: errors.ErrVATInvalid, StatusCode: http.StatusBadRequest}
	}
	if err := a.UserService.SetVatIn(uid, req.VatIn); err != nil {
		return nil, err
	}
	return &latencyAt.StatusOK, nil
}
