package http

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"

	"github.com/julienschmidt/httprouter"
	"github.com/prometheus/client_golang/prometheus"

	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"
)

var (
	requestCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "lat",
			Name:      "http_requests_total",
			Help:      "Total number of requests by handler",
		},
		[]string{"method", "path"},
	)
	requestDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: "lat",
			Name:      "http_request_duration_seconds",
			Help:      "Duration of request by handler histogram",
			Buckets:   prometheus.ExponentialBuckets(0.001, 10, 5),
		},
		[]string{"method", "path"},
	)
	errorCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "lat",
			Name:      "http_errors_total",
			Help:      "Total number of error in http interface",
		},
		[]string{"method", "path"},
	)
)

func init() {
	prometheus.MustRegister(requestCounter)
	prometheus.MustRegister(requestDuration)
	prometheus.MustRegister(errorCounter)
}

// Handler is a collection of all the service handlers.
type Handler struct {
	*APIHandler
	FileHandler  http.Handler
	staticDir    string
	AuthUser     string
	AuthPassword string
}

// ServeHTTP delegates a request to the appropriate subhandler.
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"method": r.Method,
		"path":   r.URL.Path,
		"client": r.RemoteAddr,
	})
	logger.Debugln(r.Method, r.URL.Path)
	switch {
	case strings.HasPrefix(r.URL.Path, "/api"):
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Authorization, Content-Type")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
		w.Header().Set("Content-Type", "application/json")
		h.APIHandler.ServeHTTP(w, r)
	case strings.HasPrefix(r.URL.Path, "/static"):
		http.StripPrefix("/static", h.FileHandler).ServeHTTP(w, r)
	case strings.HasPrefix(r.URL.Path, "/-/health"):
		h.APIHandler.HandleHealth(w, r)
	case r.URL.Path == "/":
		h.handleIndex(w, r)
	default:
		r.URL.Path = "/"
		h.FileHandler.ServeHTTP(w, r)
	}
}

// errorResponse is a generic response for sending a error.
type errorResponse struct {
	Err string `json:"err,omitempty"`
}

func Error(w http.ResponseWriter, err error, code int) {
	// Log error.
	logger := log.WithFields(log.Fields{
		"code":  strconv.Itoa(code),
		"error": err.Error(),
	})

	log.Printf("http error: %s (code=%d)", err, code)

	// Hide error from client if it is internal.
	if code == http.StatusInternalServerError {
		logger.Error(err)
		err = errors.ErrInternal
	} else {
		logger.Info(err)
	}
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(&latencyAt.Status{Status: "error", Message: err.Error()})
}

type HandlerErrFunc func(r *http.Request, ps httprouter.Params) (interface{}, error)

func HandleError(method, path string, fn HandlerErrFunc) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		start := time.Now()
		v, err := fn(r, ps)
		if err != nil {
			log.Println(err)
			status := http.StatusInternalServerError
			lerr, ok := err.(*errors.Error)
			if ok && lerr.StatusCode != 0 {
				status = lerr.StatusCode
			}
			w.WriteHeader(status)
			json.NewEncoder(w).Encode(&latencyAt.Status{Status: "error", Message: err.Error()})

			// 402/403 aren't errors, so do not count em
			if status == http.StatusForbidden || status == http.StatusPaymentRequired {
				return
			}
			errorCounter.WithLabelValues(method, path).Inc()
			return
		}
		requestDuration.WithLabelValues(method, path).Observe(time.Since(start).Seconds())

		if err := json.NewEncoder(w).Encode(v); err != nil {
			log.Println(err)
			Error(w, err, http.StatusInternalServerError)
			return
		}
	}
}
