package postgres

import (
	"log"
	"reflect"
	"testing"
	"time"

	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"
)

func TestNewTokenService(t *testing.T) {
	db, err := initDB()
	assert(t, err)

	ts, err := NewTokenService(db)
	assert(t, err)
	defer ts.Purge()
	assert(t, ts.Purge())
	us, err := NewUserService(db)
	assert(t, err)
	defer us.Purge()
	assert(t, us.Purge())
	user := &latencyAt.User{
		Email:        "foobar@example.com",
		PasswordHash: []byte("abc"),
		Activated:    false,
		Balance:      1234,
	}
	assert(t, us.Create(user))

	tokens := []latencyAt.Token{
		latencyAt.Token{
			ID:      0, // gets filled by CreateToken()
			Token:   "foobar23425",
			UserID:  user.ID,
			Created: time.Date(2015, 2, 13, 0, 0, 0, 0, time.UTC).Truncate(time.Millisecond),
		},
		latencyAt.Token{
			ID:      0, // gets filled by CreateToken()
			Token:   "shsfghsfdghdfssg",
			UserID:  user.ID,
			Created: time.Date(2015, 2, 13, 0, 0, 0, 0, time.UTC).Truncate(time.Millisecond),
		},
	}

	for _, step := range []func() error{
		func() error {
			return ts.CreateToken(&tokens[0])
		},
		func() error {
			return ts.CreateToken(&tokens[1])
		},
	} {
		if err := step(); err != nil {
			t.Fatal(err)
		}
		tk := &tokens[0]
		ret, err := ts.TokenByToken(tk.Token)
		if err != nil {
			t.Fatal(err)
		}
		ret.Created = ret.Created.In(tk.Created.Location())
		if !reflect.DeepEqual(tk, ret) {
			t.Fatalf("\na> %#v\nb> %#v", tk, ret)
		}
	}

	rtokens, err := ts.TokensByUserID(user.ID)
	assert(t, err)
	for i, t := range rtokens {
		t.Created = t.Created.In(tokens[i].Created.Location())
	}
	if !tokensEq(tokens, rtokens) {
		t.Fatalf("\na> %#v\nb> %#v", tokens, rtokens)
	}
	for _, tk := range tokens {
		assert(t, ts.DeleteToken(&tk))
	}
	for _, tk := range tokens {
		_, err := ts.TokenByToken(tk.Token)
		if err != errors.ErrTokenNotFound {
			t.Fatalf("Expected %v to be gone", tk)
		}
	}
}

func tokensEq(a, b []latencyAt.Token) bool {
	if len(a) != len(b) {
		return false
	}
	for i, ae := range a {
		be := b[i]
		be.Created = be.Created.In(ae.Created.Location())
		if !reflect.DeepEqual(ae, be) {
			log.Printf("\na> %#v\nb> %#v", ae, be)
			return false
		}
	}
	return true
}
