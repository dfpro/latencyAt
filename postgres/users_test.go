package postgres

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"

	_ "github.com/lib/pq"
)

func TestNewUserService(t *testing.T) {
	db, err := initDB()
	assert(t, err)

	us, err := NewUserService(db)
	assert(t, err)
	assert(t, us.Purge())
	defer us.Purge()

	es, err := NewEmailTokenService(db)
	assert(t, err)
	assert(t, es.Purge())
	defer es.Purge()

	et := &latencyAt.EmailToken{
		ID:      0, // gets filled by CreateEmailToken()
		Token:   "foobar23425",
		Created: time.Now().Truncate(time.Millisecond).In(time.UTC),
	}
	assert(t, es.CreateEmailToken(et))

	if err := us.Create(&latencyAt.User{ID: 23}); err == nil {
		t.Fatal("Expected error when creating user with ID")
	}
	for _, fn := range []func() error{
		func() error { _, err := us.UserByEmail("foo@bar.com"); return err },
		func() error { _, err := us.User(23); return err },
	} {
		if err := fn(); err != errors.ErrUserNotFound {
			t.Fatalf("Expected error %v but got %v", errors.ErrUserNotFound, err)
		}
	}

	lastBilling := time.Time{}.UTC()
	user := &latencyAt.User{
		Email:        "foobar@example.com",
		EmailTokenID: &et.ID,
		PasswordHash: []byte("abc"),
		Activated:    false,
		Balance:      1234,
		LastBilling:  &lastBilling,
	}

	for i, step := range []func(u *latencyAt.User) error{
		func(u *latencyAt.User) error {
			u.Registered = time.Now().In(time.UTC).Truncate(time.Millisecond)
			return us.Create(user)
		},
		func(u *latencyAt.User) error {
			u.Activated = true
			return us.Activate(u)
		},
		func(u *latencyAt.User) error {
			u.Balance = 1000
			return us.BalanceAbs(u, 1000)
		},
		func(u *latencyAt.User) error {
			u.Balance += 1
			return us.Balance(u, 1)
		},
		func(u *latencyAt.User) error {
			u.Balance -= 1
			return us.Balance(u, -1)
		},
		func(u *latencyAt.User) error {
			u.Activated = false
			u.Email = "bazbar@example.com"
			return us.SetEmailAndDeactivate(u.ID, u.Email)
		},
		func(u *latencyAt.User) error {
			ruser, err := us.UserByEmailTokenID(et.ID)
			assert(t, err)

			lb := ruser.LastBilling.UTC()
			ruser.LastBilling = &lb
			ruser.Registered = ruser.Registered.In(user.Registered.Location())
			if !reflect.DeepEqual(user, ruser) {
				t.Fatalf("DeepEqual(user, ruser):\na> %#v\nb> %#v", user, ruser)
			}

			u.PasswordHash = []byte("blafasel")
			u.EmailTokenID = nil
			return us.SetPasswordHash(u.ID, u.PasswordHash)
		},
		func(u *latencyAt.User) error {
			u.StripeCustomerID = "tok-abc"
			return us.SetStripeCustomerID(user.ID, u.StripeCustomerID)
		},
	} {
		t.Log("Step", i)
		assert(t, step(user))
		userByMail, err := us.UserByEmail(user.Email)
		assert(t, err)
		// libpq parses times without zone weirdly, so converting it to UTC.
		// See: https://github.com/lib/pq/issues/329
		lb := userByMail.LastBilling.UTC()
		userByMail.LastBilling = &lb
		userByID, err := us.User(user.ID)
		assert(t, err)
		lb = userByID.LastBilling.UTC()
		userByID.LastBilling = &lb
		if !reflect.DeepEqual(userByMail, userByID) {
			t.Fatalf("DeepEqual(userByMail, userByID):\na> %#v\nb> %#v", userByMail, userByID)
		}

		userByID.Registered = userByID.Registered.In(user.Registered.Location())
		if !reflect.DeepEqual(user, userByID) {
			t.Fatalf("DeepEqual(user, userByID):\na> %#v\nb> %#v", user, userByID)
		}
	}
}
