package subs

import (
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	stripe "github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/customer"
	"github.com/stripe/stripe-go/event"
	"github.com/stripe/stripe-go/invoice"
	"github.com/stripe/stripe-go/token"

	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"
	"gitlab.com/latency.at/latencyAt/mock"
)

func init() {
	stripe.Key = os.Getenv("STRIPE_KEY")
}

func TestSubscribe(t *testing.T) {
	if stripe.Key == "" {
		t.Skip("No STRIPE_KEY set, skipping")
	}
	user := &latencyAt.User{
		ID:      23,
		Email:   "test+units@latency.at",
		Balance: 5000,
	}
	card := &stripe.CardParams{
		Number: "4242424242424242",
		Month:  "5",
		Year:   "2020",
		CVC:    "1234",
	}
	testSubscribe(t, card, user, 0.0)
}

func TestSubscribeEUIndividual(t *testing.T) {
	if stripe.Key == "" {
		t.Skip("No STRIPE_KEY set, skipping")
	}

	user := &latencyAt.User{
		ID:      23,
		Email:   "test+units@latency.at",
		Balance: 5000,
	}
	card := &stripe.CardParams{
		Number: "4000002500000003", // FR
		Month:  "5",
		Year:   "2020",
		CVC:    "1234",
	}
	testSubscribe(t, card, user, 0.2)
}

func TestSubscribeEUBusiness(t *testing.T) {
	if stripe.Key == "" {
		t.Skip("No STRIPE_KEY set, skipping")
	}

	user := &latencyAt.User{
		ID:      23,
		Email:   "test+units@latency.at",
		Balance: 5000,
		VatIn:   "FR00012345",
	}
	card := &stripe.CardParams{
		Number: "4000002500000003", // FR
		Month:  "5",
		Year:   "2020",
		CVC:    "1234",
	}
	testSubscribe(t, card, user, 0.0)
}

func cleanUp(email string) {
	i := customer.List(nil)
	for i.Next() {
		c := i.Customer()
		if c.Email != email {
			continue
		}
		log.Println("cleanup: deleting", c)
		_, err := customer.Del(c.ID)
		if err != nil {
			panic(err)
		}
	}
	if err := i.Err(); err != nil {
		panic(err)
	}
}

type testSubscriptionService struct {
	*SubscriptionService

	CustomerID string
}

func newSubscriptionService(t *testing.T, user *latencyAt.User) *testSubscriptionService {
	tss := &testSubscriptionService{}

	us := &mock.UserService{
		UserFn: func(id int) (*latencyAt.User, error) {
			if id != user.ID {
				t.Fatalf("expected: %d, got: %d", user.ID, id)
			}
			return user, nil
		},
		UserByEmailFn: func(email string) (*latencyAt.User, error) {
			if email != user.Email {
				t.Fatalf("expected: %s, got: %s", user.Email, email)
			}
			return user, nil
		},
		SetStripeCustomerIDFn: func(id int, cid string) error {
			if id != user.ID {
				t.Fatalf("expected: %d, got: %d", user.ID, id)
			}
			tss.CustomerID = cid
			return nil
		},
		BalanceAbsByStripeCustomerIDFn: func(cid string, balance int) error {
			if cid != tss.CustomerID {
				t.Fatalf("expected: %s, got: %s", tss.CustomerID, cid)
			}
			user.Balance = balance
			t.Log("Set balance =", balance)
			return nil
		},
		BalanceByStripeCustomerIDFn: func(cid string, balance int) error {
			if cid != tss.CustomerID {
				t.Fatalf("expected: %s, got: %s", tss.CustomerID, cid)
			}
			user.Balance += balance
			t.Log("Set balance +=", balance)
			return nil
		},
	}
	events := map[string]*latencyAt.Event{}
	es := &mock.EventService{
		GetFn: func(key string) (*latencyAt.Event, error) {
			e := events[key]
			if e == nil {
				return nil, errors.ErrEventNotFound
			}
			return e, nil
		},
		AddTxFn: func(e *latencyAt.Event) (latencyAt.Tx, error) {
			return &mockTx{
				CommitFn: func() error {
					events[e.Key] = e
					return nil
				},
				RollbackFn: func() error {
					return nil
				},
			}, nil
		},
	}
	ss, err := NewSubscriptionService(us, es, "fixtures/vat_rates.csv")
	if err != nil {
		t.Fatal(err)
	}
	tss.SubscriptionService = ss
	return tss
}

func testSubscribe(t *testing.T, card *stripe.CardParams, user *latencyAt.User, expectedTax float64) {
	cleanUp(user.Email)
	ss := newSubscriptionService(t, user)
	// defer cleanUp(user.Email)

	// Add new card
	token, err := token.New(&stripe.TokenParams{
		Card: card,
	})
	if err != nil {
		t.Fatal(err)
	}

	for _, err := range []error{
		func() error { return ss.DeleteCard(user.ID) }(),
		func() error {
			_, err := ss.GetCard(user.ID)
			return err
		}(),
	} {
		lerr, ok := err.(*errors.Error)
		if !ok || lerr.Err != errors.ErrCardNotFound {
			t.Fatal("Expected error", errors.ErrCardNotFound, "but got", err)
		}
	}

	if err := ss.AddCard(user.ID, token.ID); err != nil {
		t.Fatal(err)
	}
	if ss.CustomerID == "" {
		t.Fatal("Expected to have customerID set")
	}

	// Initial subscription
	if err := ss.Subscribe(user.ID, "starter"); err != nil {
		t.Fatal(err)
	}

	// FIXME: Here we would in reality handle a webhook
	ep := eventProcessor{
		ss:         ss.SubscriptionService,
		T:          t,
		CustomerID: ss.CustomerID,
		processed:  map[string]bool{},
	}
	ep.process()
	if user.Balance != 1000000 {
		t.Fatalf("expected balance %d, actual balance %d", 1000000, user.Balance)
	}
	assertInvoice(t, ss.CustomerID, expectedTax, 1500)

	// Delete card fails
	err = ss.DeleteCard(user.ID)
	if err == nil {
		t.Fatalf("expected error but got %v", err)
	}
	if lerr, ok := err.(*errors.Error); ok {
		if lerr.StatusCode != http.StatusConflict {
			t.Fatalf("Expected status %d, but got: %#v", http.StatusConflict, lerr)
		}
	} else {
		t.Fatal("expected errors.Error")
	}

	// Upgrade
	if err := ss.Subscribe(user.ID, "startup"); err != nil {
		t.Fatal(err)
	}

	// FIXME: process 3x:
	// - first time will create invoice
	// - second time will trigger play
	// - third time process it
	ep.process()
	ep.process()
	ep.process()
	t.Log(user)
	if user.Balance != 6000000 {
		t.Fatalf("expected balance %d, actual balance %d", 6000000, user.Balance)
	}
	assertInvoice(t, ss.CustomerID, expectedTax, 5000-1500, 1500) // prorated charge

	// Downgrade
	if err := ss.Subscribe(user.ID, "starter"); err != nil {
		t.Fatal(err)
	}
	// run a few times to make sure we get 'unexpected' events
	ep.process()
	ep.process()
	ep.process()

	// same balance...
	if user.Balance != 6000000 {
		t.Fatalf("expected balance %d, actual balance %d", 6000000, user.Balance)
	}
	// no new invoice
	assertInvoice(t, ss.CustomerID, expectedTax, 5000-1500, 1500)

	// Unsubscribe
	if err := ss.Unsubscribe(user.ID); err != nil {
		t.Fatal(err)
	}
	subs, err := ss.GetSubscription(ss.CustomerID)
	if err != nil {
		t.Fatal(err)
	}
	if subs != nil {
		t.Fatal("Expected no subscription but found:", subs)
	}

	// Delete Card
	if err := ss.DeleteCard(user.ID); err != nil {
		t.Fatal(err)
	}
}

type eventProcessor struct {
	ss *SubscriptionService
	*testing.T
	CustomerID string
	processed  map[string]bool
}

func (ep *eventProcessor) process() {
	elp := &stripe.EventListParams{}
	elp.Filters.AddFilter("created", "gt", strconv.Itoa(int(time.Now().Add(-5*time.Minute).Unix())))
	i := event.List(elp)
	for i.Next() {
		e := i.Event()
		switch {
		case strings.HasPrefix(e.Type, "customer.subscription"):
			subs := &stripe.Sub{}
			if err := subs.UnmarshalJSON(e.Data.Raw); err != nil {
				ep.Fatal(err)
			}
			if subs.Customer.ID != ep.CustomerID {
				continue
			}
		case strings.HasPrefix(e.Type, "invoice"):
			iv := &stripe.Invoice{}
			if err := iv.UnmarshalJSON(e.Data.Raw); err != nil {
				ep.Fatal(err)
			}
			if iv.Customer.ID != ep.CustomerID {
				continue
			}
		default:
			continue
		}

		if err := ep.ss.HandleEvent(e); err != nil {
			ep.Fatal(err)
		}
	}
	if err := i.Err(); err != nil {
		ep.Fatal(err)
	}
}

func assertInvoice(t *testing.T, id string, taxPercent float64, amount ...int) {
	params := &stripe.InvoiceListParams{Customer: id}
	i := invoice.List(params)
	n := 0
	k := len(amount)
	for i.Next() {
		iv := i.Invoice()
		if n > k {
			t.Fatalf("Found more than the expected %d invoices", k)
		}
		if iv.TaxPercent != taxPercent*100 {
			t.Fatalf("Expected %f TaxPercent but got %f", taxPercent*100, iv.TaxPercent)
		}
		amount := float64(amount[n])
		awt := int64(amount + (amount * taxPercent))
		if iv.Amount != awt {
			t.Fatalf("Expected invoice over %d cent but got %d in invoice %#v", awt, iv.Amount, iv)
		}
		n++
	}
	if n != k {
		t.Fatalf("Expected %d but got %d invoices", k, n)
	}
	if err := i.Err(); err != nil {
		t.Fatal(err)
	}
}

type mockTx struct {
	CommitFn   func() error
	RollbackFn func() error
}

func (t *mockTx) Rollback() error {
	return t.RollbackFn()
}

func (t *mockTx) Commit() error {
	return t.CommitFn()
}
